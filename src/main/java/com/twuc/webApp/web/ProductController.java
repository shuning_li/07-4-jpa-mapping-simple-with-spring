package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    ProductRepository repo;

    @PostMapping("")
    public ResponseEntity<Object> createProduct(@Valid @RequestBody CreateProductRequest createProductRequest) {
        String name = createProductRequest.getName();
        Integer price = createProductRequest.getPrice();
        String unit = createProductRequest.getUnit();

        Product savedProduct = repo.save(new Product(name, price, unit));

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location", String.format("http://localhost/api/products/%d", savedProduct.getId()))
                .build();
    }

    @GetMapping("/{id}")
    public GetProductResponse getProduct(@PathVariable Long id) {
        Product product = repo.getOne(id);
        return new GetProductResponse(product);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    }

    @ExceptionHandler({EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleEntityNotFoundException(EntityNotFoundException e) {
    }
}